# Saw Loser - 2 Saw 2 Lose

## Introduction

You can find the [old Saw Loser repo here](https://gitlab.com/alexmordue/saw-loser) if you want to take a look at that.

Saw Loser - 2 Saw 2 Lose is the second in the Saw Loser line of robots, it has quite a lot of differences and some similarities to the original.

## Design

I've created a [document about the design process](DESIGN.md) for this version, why I did things the way I did, hopefully you find it interesting.

If you'd just like to see/download the design [click here to view the Fusion 360 model](https://a360.co/42ODAst). The model is not fully joined up and contains quite a few extraneous parts, but should be enough to get the gist.

## Electronics

## Manufacturing

## Assembly