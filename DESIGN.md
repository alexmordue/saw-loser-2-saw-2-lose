# Design

[[_TOC_]]

## Introduction

This time I wanted something that was primarily a hammer-saw, rather than the vert on a stick of the first Saw Loser, for that to happen it needed a more robust spinner and a much faster actuating arm (along with the hammer-saw standard of keeping the wepon out the back until fired).

While there are 3d printed parts in this design, I wanted to increase the durability so that I didn't need to replace the chassis and other large parts during a competition, as such I wanted to keep 3d printing only for smaller and "consumable" parts.

The drive system from Saw Loser 1 was very reliable as such I wanted to keep that fairly similar.

## Spinner

I wanted to stick with a hub motor for the weapon, the spinner in SL1 was fairly powerful but quite large, delicate in places and a pain to maintain. So when I decided to collaborate on a beetle clusterbot with Mark Leigh, of K2 fame, I decided to make a fully machined hub motor spinner for the little 4wd vert. I was happy with the single supported side design on Saw Loser 1, so decided to continue with that, if nothing else it adds some flex to the system to prevent the spinner and bearings from taking too high a shock load (since the arm will twist, hopefully elastically, on big hits), I'd rather replace the arm than spinner parts.

The cluster bot ["Wee Loser"](https://www.facebook.com/teamrgbrobots/posts/pfbid02e3LVNibLMPhohEGb5amUt84myJrYU9k9npoeSar1i9fiJVH4A3XFjS719ewoURCbl) worked really well, with the hub motor working perfectly throughout its one and only competition, despite firing itself into the ceiling multiple times. The design used for Wee Loser is essentially unchanged in SL2 so I will be talking about them interchangeably.

As I had recently got access to a 3040 CNC router, I decided to try (perhaps foolishly) to machine the aluminium parts for the spinner. For anyone familiar with CNC routers, you want to try and keep the number of setups for a single part as low as possible and ideally just a single sided setup (2.5D milling). I took inspiration from Cosmin Gorgovan's ([BTR](https://www.facebook.com/BadTimingRobotics)) Daedalus, which makes use of a hub motor in a huge horizontal. The clever thing about it was that all the parts were possible to make on a router with a single sided setup, I decided to see if I could do the same.

Only using single sided setups does lead to the this hub motor being made in 5 parts (2 rotors with bearing cups, 2 part shaft, 1 part to join the rotors and contain the brushless rotor), whereas with a multi-sided setup you could probably get away with just 3 parts (2 part shaft and a complex rotor that includes 2 bearing pockets and pocket for the brushless rotor).

This cross sectional view hopefully give you some idea of how the spinner is put together.
![Spinner - Cross section](images/design%20-%20spinner%20-%20crosssection.jpg)

### The non-spinning parts
![Spinner - static parts](images/design%20-%20spinner%20-%20stator.jpg)

The aluminium half shafts do not meet in the middle and we instead just put the stator into the stack. This hasn't caused a failure yet, but it's a potential weak point. Also the model of the stator is not quite accurate, the half shafts push in to the stator where it previously had bearings (for the motors original 5mm shaft). The section of the stator in between the two half shafts is thicker than pictured.

The half of the "shaft" that mates to the arm is the only part that needs some "machining" from a second side, to provide counterboring for the bolts that hold the brushless motor's stator in place. This was possible with just a drill.

This half shaft goes in to a matching hexagonal pocket in the arm, with the shoulder bolt through the arm and the whole spinner assembly, to hold it all together.

### The spinning parts
![Spinner - rotating parts](images/design%20-%20spinner%20-%20rotor.jpg)

The weapon disk, the rotors and the "locating hub" (which also has the pocket for the motor rotor), have hexagonal shapes which key the parts together. The 5 x m5 HT bolts just hold the parts together laterally, and should not transfer rotational energy.


### The exploded view
![Spinner - Exploded view](images/design%20-%20spinner%20-exploded.mp4)

## Arm

The arm on SL1 was very much geared as a lifter, using a worm drive. For SL2, the arm needed to move much faster, as a hammersaw needs a high impact speed to get lots of engagement with the spinner.

I wanted the arm assembly for SL2 to be separate from the main chassis, as was done for SL1 (with a lot of drawbacks).

There are two uprights with two barrel nuts in each which are bolted through from the bottom of the chassis to keep it all together.

![Arm - Full Assembly](images/design%20-%20arm%20-%20assembly%20top%20left.png)

The arm mechanism in the version of Saw Loser 2 in this writeup has seen a number of design iterations from the first version of Saw Loser 2.

The motor and gear train has had the following revisions:
1) Brushless 22mm Rotalink with mod1, 5mm wide, straight cut pinion (held with a m4 grub and half square nut) and 8mm arm gear (both printed nylon)
2) Brushless 22mm Rotalink with mod1.6 helical gears, again 3d printed nylon, 5mm and 8mm wide
3) Megaspark with double supported shaft, mod1.6 helical gears, 15mm wide pinion (held on with an m5 grub and half square nut) and 12mm wide arm gear

For revision 1 it worked but the teeth stripped very easily, this version was never run in a competition, as I discovered the teeth stripped in testing very easily.

For revision 2 I ran this for a few competitions, including Scouse Showdown 1 and Norwalk May '22. It mostly worked, but was still stripping teeth every fight or two. There were a couple of things causing this, being single supported the gearmotor shaft could move a fair amount under load, also the pinion itself was fairly thin, so the cross sectional area just wasn't really big enough. Also the nylon pinion would eventually round out on the 4mm Rotalink shaft.

For revision 3 (the current revision) I wanted something where I wouldn't have to replace any gears during an event due to it breaking itself (fair enough if broken by another competitor). 

The current revision uses a Megaspark, as it has a nice long 6mm shaft, which can easily be supported on the non-gearbox side, and a much larger grub screw can be used, also with the added length the pinion itself can be much wider, all these factors lead to a pinion which is much less likely to lose teeth or slip on the shaft. 

The disadvantage of going to the Megaspark is that it was much longer that the Rotalink 22mm conversions I had been using, so in order to keep the dimensions on the robot the same (to reuse the chassis/lids), the Megaspark had to poke most of the way through the right hand weapon bulkhead and have a thin aluminium mounting plate.

Previously when in the retracted position the arm sat alongside the pinion, with a couple of mm of clearance between the two, inside the chassis. With a full width pinion the arm needed to be moved up, out of the chassis, hence the "bend" around the pinion that can be seen in the design now:

![Arm - Bottom Left](images/design%20-%20arm%20-%20assembly%20bottom%20left.png)

The arm itself is made of routed HDPE. It has been gradually beefed up in places to where it mostly doesn't plastically deform on hits (The spinner is single supported so the older arms tended to get a twist in them over time).

Another notable design feature of the arm assembly is that the HDPE arm sits in a printed TPU cup, inside the nylon printed arm gear. In older versions there was just a 3d printed gear bolted to the arm. The intention of this newer, less firmly coupled design was to hopefully reduce shock loads to the gear train. It also makes changing the gear very easy as you only have to take out the main arm shaft (With the previous design where the gear was bolted to the HDPE arm, the heat set inserts could sometimes end up loosening and spinning in the plastic, making replacement of the gear even more painful).

Overall I'm happy with the most recent revision of the arm and it is yet to break itself.

## Drive

I was very happy with the 4wd setup from SL1, consisting of two printed hubs (with polyurethane molded wheels) with integrated gears, on deadshafts, with the pinion on a rotalink brushless conversion, in between the pair. 

The main changes are:
1) 6mm shoulder bolt deadshafts - 5mm shoulder bolts are a pain to get, and pretty expensive, also usually only available in stainless.
2) Bronze bushings instead of bearings in the wheels, for weight and price.
3) The wheel cores are TPU now (rather than nylon) and large enough diameter that if the cast PU tyre comes off totally, the robot can still run on the cores.
4) Rather than threading the deadshafts into heat set inserts, instead I now use m5 square weld nuts in square-ish slots in the body. Heat set inserts have a tendency to just spin in HDPE, and are almost impossible to replace.
5) The drive gearboxes are both clamped and face mounted, as opposed to just face mounted in SL1 (I have not modelled the holes for face mounting, it's probably not really needed but belt and braces).

![Arm - Bottom Left](images/design%20-%20drive.png)

## Chassis

For SL1 I had a fully 3d printed single piece nylon chassis with titanium lids. This worked okay at the time, but had a number of drawbacks.

1. It tended to fail at any place where there was something going through the chassis:
    1. Deadshafts
    2. Armor mounting points
    3. Arm pivot shaft
    4. Lid mounting points
2. It was hard to print:
    1. Warping
    2. Potions to prevent warping caused the nylon to be forever adhered to the glass bed
    3. Multiple glass beds were destroyed trying to remove chassis
    4. Warping

These two things together meant that for Saw Loser 2 I wanted to try something that might be more durable and repeatably manufacturable.

![Chassis](images/design%20-%20chassis.png)

I opted for a billet HDPE chassis since I'd already had some success with that technique for my cluster bot "Wee Loser".

I also opted to have separate weapon uprights from the chassis so that the weapon assembly could be swapped out if/when it got damaged.

Both the top and bottom chassis parts require 2 sided setups as otherwise the design was just too chunky.

The horizontal holes for the deadshafts and motor were made using 3d printed jigs that slot into the gearmotor pocket. 

![Chassis - drillguide](images/design%20-%20chassis%20-%20drillguide.png)

The left side top somewhat flows over the components hence the stepped shape as it goes, from front to back, over the battery, drive gearmotor and electronics.

The right side top just keeps the same height from front to back, as it has to go over two gearmotors, so there is a big empty section at the front (air is the best armor).

The tops and bottom are held together with M4 bolts that go into heat set inserts, for this application they work okay as the bolts are not torqued up very tight at all.

The weapon uprights are bolted in via barrel nuts, so would take an enourmous amount of almost certainly terminal damage to remove.

## Wedge/Forks

For mounting the front armour set ups I just used very chunky threaded inserts, probably designed for furniture making (screwing into wood). These are pretty tough to pull out, I haven't had a failure yet, but I suspect it's only a matter of time.

![Armour mount](images/design%20-%20armour%20-%20mount.png)

The actual mounts themselves are TPU, bolted into the inserts, in shallow pockets in the chassis, this probably helps with the potential issue of them ripping out. 

The shaft that goes through the mounts for the fork or wedge to rotate on is just a 5mm stainless shoulder bolt (spares from SL1 drive deadshafts), so that probably also helps somewhat, as they tend to deform quite easily, especially with the AR400 forks.

The wedge is a huge chunk (200g or so) of TPU, when I went to the US I had to basically leave this hollow, hence the incredibly floppy wedge at NHRL. It now has a "butt crack" in the middle to help reduce self inflicted hits.

![Armour wedge](images/design%20-%20armour%20-%20wedge.png)

The forks are not particularly agressive, I don't want to get stuck together on another robot. The ears at the top cause beaters to flick the fork up and this tends to make the robots fly away from each other, there's probably a better design , but I have a load of these to use up first...